<?php
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'pixel';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failsed: " . $conn->connect_error);
}

// Insert data into the employee table
if (isset($_POST["create"])) {
  $firstName = $_POST["first-name"];
  $lastName = $_POST["last-name"];
  $middleName = $_POST["middle-name"];
  $birthday = $_POST["birthday"];
  $address = $_POST["address"];

  $sql = 'INSERT INTO employee (first_name, last_name, middle_name, birthday, address)
          VALUES (?, ?, ?, ?, ?)';

  $stmt = $conn->prepare($sql);
  $stmt->bind_param('sssss', $firstName, $lastName, $middleName, $birthday, $address);
 
  if ($stmt->execute() === TRUE) {
      echo "New record created successfully<br><br>";
  } else {
      echo "Error: " . $sql . "<br><br>" . $conn->error;
  }

  $conn->close();
}


// retrieve the first name, last name, and birthday of all employees in the table
if (isset($_GET["read"])) {
  $sql = 'SELECT first_name, last_name, middle_name, birthday
          FROM employee';
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
          echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
      }
      echo "<br>";
  } else {
      echo "0 results<br>";
  }

  $conn->close();
}


//  retrieve the first name, last name, and birthday of the employee with the highest ID number
if (isset($_GET["read-id"])) {
  $id = $_GET["get-user-id"];

  $sql =  'SELECT first_name, last_name, birthday
          FROM employee
          WHERE id = ?';

  $stmt = $conn->prepare($sql);
  $stmt->bind_param('i', $id);
  $stmt->execute();

  $stmt->bind_result($firstName, $lastName, $birthday);

  $found = false;
  while ($stmt->fetch()) {
    $found = true;
    echo "First Name: " . $firstName . " - Last Name: " . $lastName . " - Birthday: " . $birthday . "<br>";
  }

  if (!$found) {
    echo "No results found!";
  }

  $conn->close();
}


// Update data in the employee table
if (isset($_POST["update-user"])) {
  $firstName = $_POST["first-name"];
  $lastName = $_POST["last-name"];
  $middleName = $_POST["middle-name"];
  $birthday = $_POST["birthday"];
  $address = $_POST["address"];
  $id = $_POST["id"];

  $sql = 'UPDATE employee
          SET first_name = ?, last_name = ?, middle_name = ?, birthday = ?, address = ? 
          WHERE id = ?';

  $stmt = $conn->prepare($sql);
  $stmt->bind_param('sssssi', $firstName, $lastName, $middleName, $birthday, $address, $id);

  if ($stmt->execute() === TRUE) {
      if ($stmt->affected_rows > 0) {
            echo "Record updated successfully<br><br>";
        } else {
            echo "No user found with the given ID.<br><br>";
        }
  } else {
      echo "Error: " . $sql . "<br><br>" . $conn->error;
  }

  $conn->close();
}


// Delete data from the employee table
if (isset($_POST["delete-id"])) {
  $id = $_POST["id"];

  $sql = 'DELETE FROM employee
          WHERE id = ?';

  $stmt = $conn->prepare($sql);
  $stmt->bind_param('i', $id);  

  if ($stmt->execute() === TRUE) {
      if ($stmt->affected_rows > 0) {
            echo "Record deleted successfully";
        } else {
            echo "No user found with the given ID.<br><br>";
        }
  } else {
      echo "Error deleting record: " . $conn->error;
  }
  $conn->close();
}

